Provision tls certificates through [Let's Encrypt](https://letsencrypt.org/)
with [lego](https://github.com/xenolf/lego).

# Volumes
- `/var/lib/lego/.lego`

# Environment Variables
## CERT_CA
- default: "https://acme-v01.api.letsencrypt.org/directory"

Which Acme Endpoint to use.

## CERT_ACME_EMAIL
Email to use in the acme account.

## CERT_DOMAIN
The domain the certificate uses.

## CERT_SAN
SAN Domains to be added to the certificate (uses ';' as seperator).

## CERT_DNS_PROVIDER
One of the list here: https://github.com/xenolf/lego/tree/master/providers/dns

## CERT_UID
What uid the certs have.

## CERT_GID
What gid the certs have.

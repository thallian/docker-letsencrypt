FROM golang:alpine as builder

RUN apk --no-cache add git
RUN go get -v -u github.com/xenolf/lego

FROM registry.gitlab.com/thallian/docker-alpine-s6:master

COPY --from=builder /go/bin/lego /bin/lego

RUN addgroup -g 2222 lego
RUN adduser -u 2222 -h /var/lib/lego -D -G lego lego

RUN apk --no-cache add ca-certificates

ADD /rootfs /

ENV CERT_HOME /var/lib/lego

VOLUME /var/lib/lego/.lego

